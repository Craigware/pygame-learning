import pygame
from tilemapEditorClasses import *

cell_size = 16;
world_size = Vector2(320,180);
world_size_cells = Vector2(world_size.x/cell_size, world_size.y/cell_size);
resolution = Vector2(1280,720);

clock = pygame.time.Clock();
target_fps = 144;
