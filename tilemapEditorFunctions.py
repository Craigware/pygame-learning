import pygame
from tilemapEditorClasses import *
import settings
from math import floor

def get_tile_subimg(tilemap, current_tile):
    cell_size = tilemap.cell_size;
    tile = tilemap.tilemap.subsurface((cell_size*current_tile.x, cell_size*current_tile.y, cell_size, cell_size));
    return tile

def select_tile(tileset, current_tile, incrementer):
    cell_dimensions = tileset.cell_dimensions();
    if incrementer > 0:
        if current_tile.x == cell_dimensions.x and current_tile.y < cell_dimensions.y:
            current_tile.x = 0;
            current_tile.y += 1;
        elif current_tile.x != cell_dimensions.x:
            current_tile.x += 1;
    if incrementer < 0:
        if current_tile.x == 0 and current_tile.y > 0:
            current_tile.x = cell_dimensions.x;
            current_tile.y -= 1;
        elif current_tile.x != 0:
            current_tile.x -= 1;

    current_tile.x = int(current_tile.x);
    current_tile.y = int(current_tile.y);
    return current_tile;

def get_mouse_tile(scaled_tile):
    mouse_position = Vector2(pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1]);
    x = floor(mouse_position.x/scaled_tile.x);
    y = floor(mouse_position.y/scaled_tile.y);
    cell_mouse_position = Vector2(x,y);

    return cell_mouse_position;

def create_board():
    grid = pygame.Surface((settings.world_size_cells.x*settings.cell_size, settings.world_size_cells.y*settings.cell_size));
    gray = pygame.Surface((settings.cell_size, settings.cell_size));
    white = pygame.Surface((settings.cell_size, settings.cell_size));
    white.fill("gray15");
    gray.fill("gray27")
    cell_y = 0;
    while cell_y < settings.world_size_cells.y:
        cell_x = 0;
        while cell_x < settings.world_size_cells.x:
            if cell_y % 2 == 0:
                if cell_x % 2 == 0:
                    grid.blit(gray, (cell_x*settings.cell_size, cell_y*settings.cell_size));
                else:
                    grid.blit(white, (cell_x*settings.cell_size, cell_y*settings.cell_size));
            else:
                if cell_x % 2 == 1:
                    grid.blit(gray, (cell_x*settings.cell_size, cell_y*settings.cell_size));
                else:
                    grid.blit(white, (cell_x*settings.cell_size, cell_y*settings.cell_size));
            cell_x += 1;
        cell_y += 1;
    return grid;
