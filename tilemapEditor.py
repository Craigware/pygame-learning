import pygame
from sys import exit
from tilemapEditorClasses import *
from tilemapEditorFunctions import *
import settings

pygame.init();

resolution = settings.resolution;
screen = pygame.display.set_mode((resolution.x, resolution.y), pygame.RESIZABLE);

tilemap = pygame.image.load("images/tilemap_with_some_textures.png");
tileset = Tileset(tilemap, 16);
current_tile = Vector2(0,0);

scaled_tile = Vector2(
    settings.cell_size * (resolution.x/settings.world_size.x),
    settings.cell_size * (resolution.y/settings.world_size.y)
)

running = True;
created = False;

tile = pygame.Surface((settings.cell_size, settings.cell_size));
tile.fill("black");
tile = pygame.transform.scale(tile, (scaled_tile.x, scaled_tile.y));

layer_01 = pygame.Surface((resolution.x, resolution.y)).convert_alpha();
layer_01.fill((0,0,0,0));

while running:
    if pygame.event.get(pygame.QUIT):
        pygame.quit();
        running = False;
        exit();

    for event in pygame.event.get():
        if event.type == pygame.WINDOWRESIZED:
            scaled_tile = Vector2(
                round(settings.cell_size * (event.x/settings.world_size.x)),
                round(settings.cell_size * (event.y/settings.world_size.y))
            )
            #worry about later
            grid = create_board();
            grid = pygame.transform.scale(grid, (screen.get_width(), screen.get_height()));
            screen.blit(grid, (0,0));
            print(scaled_tile.x, scaled_tile.y);
            layer_01 = pygame.transform.scale(layer_01, (event.x, event.y));

            tile = pygame.transform.scale(tile, (scaled_tile.x, scaled_tile.y));

        if event.type == pygame.MOUSEBUTTONDOWN:
            if pygame.mouse.get_pressed()[0] == True:
                hovered_tile = get_mouse_tile(scaled_tile);
                layer_01.blit(tile, (hovered_tile.x * scaled_tile.x, hovered_tile.y * scaled_tile.y));

        if event.type == pygame.MOUSEWHEEL:
            if event.y != 0:
                select_tile(tileset, current_tile, event.y);
                tile = get_tile_subimg(tileset, current_tile);
                tile = pygame.transform.scale(tile, (scaled_tile.x, scaled_tile.y));

    if created == False:
        grid = create_board();
        grid = pygame.transform.scale(grid, (screen.get_width(), screen.get_height()));
        screen.blit(grid, (0,0));
        created = True;

    screen.blit(layer_01, (0,0));
    pygame.display.update();
