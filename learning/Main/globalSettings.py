import pygame

game_name = "Pygame Learning Project";

screen_height = 720; screen_width = 1280;
screen = pygame.display.set_mode((screen_width, screen_height));

cell_size = 16;
cell_height = screen_height/cell_size; cell_width = screen_width/cell_size;


target_fps = 144;
