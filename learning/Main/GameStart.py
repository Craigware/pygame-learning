import pygame, time
import globalSettings as globalSettings
from sys import exit

running = True;
screen = globalSettings.screen;
clock = pygame.time.Clock();

background = pygame.Surface((1280,720));
background.fill("antiquewhite")

square = pygame.Surface((100,100));
square.fill("blue");

prev_time = time.time();

playerX = 0; playerY = 0; gravity = 1000; move_speed = 500; jumping = False; canJump = False;
jump_height = 500; jump_speed = 1000;

while running:
    clock.tick(globalSettings.target_fps);

    now = time.time()
    dt = now - prev_time;
    prev_time = now;

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit();
            running = False;
            exit();
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                if canJump == True:
                    jumping = True;
                    canJump = False;

    keys = pygame.key.get_pressed();
    if keys[pygame.K_d]:
        playerX += move_speed * dt;
    if keys[pygame.K_a]:
        playerX -= move_speed * dt;

    if jumping:
        playerY -= jump_speed * dt;
        if playerY < globalSettings.screen_height-jump_height:
            jumping = False

    if playerY < globalSettings.screen_height-square.get_height():
        if jumping == False:
            playerY += gravity * dt;
    elif playerY > globalSettings.screen_height-square.get_height() - 10:
        canJump = True;

    screen.blit(background, (0,0));
    screen.blit(square, (playerX,playerY));


    pygame.display.update();
