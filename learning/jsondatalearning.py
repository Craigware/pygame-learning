import json

background_layers = {
}
#might be useful for entity locations and such
#might not be as useful as first thought for tilemap creation
#should probably just edit an image rather than stitch abunch together,
#should probs just use 3rd party application



#how the data should look
tiles = {
    "Layer_1": { #one dict per tile?
        "position": (0,0),
        "tile": "grass"
    }
}


level_data = {
    "background_layers": background_layers,
    "tiles": tiles
}

#saves and loads data
with open('data/data.json', 'w', encoding='utf-8') as f:
    json.dump(level_data, f, ensure_ascii=False, indent=4)

with open('data/data.json') as f:
    loaded_data = json.load(f)

print(loaded_data)
