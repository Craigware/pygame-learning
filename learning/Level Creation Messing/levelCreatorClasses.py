import pygame
from globalSettings import game_name

class LevelSettings():
    def __init__(self, background_images={}, window_caption=game_name, level_data={}):
        self.level_data = level_data;
        self.window_caption = window_caption;
        if type(background_images) == dict:
            self.background_images = background_images;
        else:
            raise ValueError("You must supply background images with a dict! You supplied a " + str(type(background_images)));


class TilemapTile():
    def __init__(self, tile=None, has_collider=False):
            self.tile = tile;
            self.has_collider = has_collider


# level_data {
#   tiles = {
#       tile,
#       layer,
#       position
#   }
#   background-images {
#       layer,
#       image path
#   }
#   foreground-images{
#       layer,
#        image path
#   }
# }
