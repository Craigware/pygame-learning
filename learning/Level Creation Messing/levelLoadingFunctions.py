import pygame
from globalSettings import screen_height, screen_width;

def background_load(level_settings, screen):
    images = level_settings.background_images;
    for layer in level_settings.background_images:
        try:
            background = pygame.image.load(images[layer]).convert_alpha();
            background = pygame.transform.scale(background, (screen_width, screen_height));
            screen.blit(background,(0,0));
            print(f"{layer} has been created.")
        except FileNotFoundError:
            print(f"LAYER ERROR : {layer}'s file was not found.\n LAYER ERROR : layer path : ({level_settings.background_images[layer]})");
