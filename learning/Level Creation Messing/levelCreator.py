import pygame
from sys import exit
import globalSettings as globalSettings; import levelLoadingFunctions;
from levelCreatorClasses import LevelSettings

pygame.init();
running = True;

screen = globalSettings.screen;

background_layers = {
                     "layer_1": "images/Background-Mountains.png",
                     "layer_2": "images/Background-Snow.png",
                     "layer_3": "images/Background-Clouds.png",
                     "layer_4": "images/Background-Mountains-2.png",
                     "layer_5": "images/Background-Mountains-3.png",
                    }


level_settings = LevelSettings(background_images=background_layers,
                                window_caption="Pygame : Level Creator",
                            );
pygame.display.set_caption(level_settings.window_caption);

background_loaded = False;

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit();
            running = False;
            exit();

    if background_loaded == False:
        levelLoadingFunctions.background_load(level_settings, screen);
        background_loaded = True;

    pygame.display.update();
    globalSettings.clock.tick(globalSettings.target_fps);
