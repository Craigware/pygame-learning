class NPC():
    def __init__(self, name, dialogue, position):
        self.name = name;
        self.dialogue = dialogue;
        self.position = position;

    def get_position(self):
        return (self.position.x, self.position.y);

    def dialogue_option(self, index):
        return self.dialogue[index];


class Vector2():
    def __init__(self, x, y):
        self.x = x;
        self.y = y;


new_vector2 = Vector2(0,0);

print(new_vector2.x);
new_NPC = NPC("Gary", ["Hey there", "it's me, Gary"], new_vector2);
print(new_NPC.get_position());
new_NPC.position.x += 1;
print(new_NPC.get_position());

print(new_NPC.dialogue_option(1));
