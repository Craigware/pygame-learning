import pygame
from pygame.locals import *

pygame.init();

screen_width = 1280; screen_height = 720;
screen = pygame.display.set_mode((screen_width,screen_height), pygame.RESIZABLE);

background = pygame.Surface((1280,720));
background.fill("orange")
scaled_amount_x = 1;
scaled_amount_y = 1;

running = True;
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False;
        if event.type == pygame.WINDOWRESIZED:
            scaled_amount_x = event.x / screen_width;
            scaled_amount_y = event.y / screen_height;
            print(1280*scaled_amount_x, 720*scaled_amount_y);
            background = pygame.transform.scale(background, (1280*scaled_amount_x, 720*scaled_amount_y));

    screen.blit(background,(0,0));
    pygame.display.update();

pygame.quit;
