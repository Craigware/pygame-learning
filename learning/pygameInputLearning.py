import pygame
from sys import exit

pygame.init();
screen_width = 1280; screen_height = 720;
screen = pygame.display.set_mode((screen_width, screen_height))
clock = pygame.time.Clock();

input_rect = pygame.Rect(200,200,140,32);

font = pygame.font.Font(None, 32);
user_input = "";
erasing = False;

run = True;

while run:
    if pygame.event.get(pygame.QUIT):
        pygame.quit();
        run = False;
        exit();

    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_BACKSPACE:
                user_input = user_input[:-1];
            else:
                user_input += event.unicode;
                print(user_input);
        if event.type == pygame.DROPFILE:
            file = event.file;
            print(file);



    screen.fill(pygame.Color(50,50,50,255));
    pygame.draw.rect(screen,(255,255,255), input_rect,2);
    text_surface = font.render(user_input,True,(255,255,255));

    # this could be a really cool dialogue effect in a game
    input_rect.w = max(100,text_surface.get_width() + 10);

    screen.blit(text_surface, (input_rect.x + 5,input_rect.y + 5));


    pygame.display.update()
    clock.tick(144);
