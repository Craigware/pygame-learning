import pygame
from sys import exit

#needed pygame init
pygame.init();

#screen resolution
screen_width = 1280;
screen_height = 720;

#set screen size
screen = pygame.display.set_mode((screen_width, screen_height));
#set window title
pygame.display.set_caption('Pygame Personal Project!');
#start pygame clock
clock = pygame.time.Clock();
#pygame.image.load('directory/file') loads an image;
test_font = pygame.font.Font(None, 50);
text_surface = test_font.render('My game', True, "White");

block_surface = pygame.Surface((100,100));
block_surface.fill('black');

#create a surface and edit its base color
background = pygame.Surface((screen_width, screen_height));
background.fill('grey23');
block_x_position = 300;
move_speed = 5;

frame_rate = 60;
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            #closes the game when you press the x button in the browser
            pygame.quit();
            #system command to exit without excuting rest of code
            exit();
    #apply surface to screen
    screen.fill("white");
    screen.blit(background,(0,0));
    screen.blit(text_surface,(screen_width*.5,screen_height*.5));
    screen.blit(block_surface,(block_x_position,320));

    if block_x_position >= screen_width:
        move_speed = -move_speed;
    elif block_x_position <= 0:
        move_speed = move_speed * -1;
    block_x_position += move_speed;


    pygame.display.update();
    #limit the framerate
    clock.tick(frame_rate);
