import json
#not json serializable
class NPC():
    def __init__(self, name="name", dialogue={}):
        self.name = name;
        self.dialogue = dialogue;

barbara_dialogue = {
    "dialogue_0": "Hello I'm Barbra",
    "dialogue_1": "I work here...",
    "dialogue_2": "Do you need something?",
}
barbara = NPC(name="barbra",dialogue=barbara_dialogue);

def NPCEncoder(npc):
    npc_description = {
        "npc_name": npc.name,
        "dialogue": npc.dialogue
    }

    return npc_description

level_data = {
    "NPC-Data": {
        "barbara": NPCEncoder(barbara),
        "barbara_2": NPCEncoder(barbara)
    },
    "Map": "images/grid",
}

level_data = json.dumps(level_data);

print(level_data);
