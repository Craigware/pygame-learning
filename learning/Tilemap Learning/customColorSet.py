from pygame import Color

transparent = Color(0,0,0,0);


gray_scale = {
    "highlight": Color(204,204,204,255),
    "light": Color(128,128,128,255),
    "dark": Color(102,102,102,255),
    "shadow": Color(26,26,26,255),
}
