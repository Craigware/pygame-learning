import pygame
from sys import exit
from math import floor
from movementTestingFunctions import move_target, move_player
from gameFunctions import create_board

pygame.init();

#game settings
screen_width = 1280; screen_height = 720;
screen = pygame.display.set_mode((screen_width,screen_height), pygame.RESIZABLE);
clock = pygame.time.Clock();
target_fps = 60;
running = True;

#world settings
cell_size = 16;
world_width = 320; world_height = 180;
scale_amount = floor(screen_width/world_width);
scale_cell_size = cell_size * scale_amount;

screen_width_cells = world_width/cell_size;
screen_height_cells = world_height/cell_size;

#player
player = pygame.Surface((cell_size,cell_size));
player.fill("yellow");
player = pygame.transform.scale(player, (player.get_width() * scale_amount, player.get_height() * scale_amount));

player_position = [0,0]; target_position = [0,0];
slide_speed = 8;

#game
while running:
    clock.tick(target_fps);

    if pygame.event.get(pygame.QUIT):
        pygame.quit();
        running = False;
        exit();

    grid = create_board(screen_width_cells, screen_height_cells, cell_size);
    grid = pygame.transform.scale(grid, (screen_width,screen_height));

    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN and player_position == target_position:
            move_target(event.key, target_position, scale_cell_size);

    move_player(player_position, target_position, slide_speed);

    screen.blit(grid, (0,0));
    screen.blit(player, player_position);
    pygame.display.update();
