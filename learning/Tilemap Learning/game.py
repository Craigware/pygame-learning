import pygame
from math import floor
from gameFunctions import create_board
import customColorSet

pygame.init();

running = True;
screen_width = 1280; screen_height = 720;
screen = pygame.display.set_mode((screen_width,screen_height));
clock = pygame.time.Clock();

world_width = 320; world_height = 180;

scale_amount = screen_width/world_width;
cell_size = 16; scaled_cell_size = cell_size * scale_amount;
screen_width_cells = world_width / cell_size;
screen_height_cells = world_height / cell_size;

red = pygame.Surface((scaled_cell_size, scaled_cell_size));
red.fill(customColorSet.gray_scale["highlight"]);

background = pygame.Surface((world_width, world_height));
layer_01 = pygame.Surface((world_width, world_height));
layer_01 = pygame.Surface.convert_alpha(layer_01);
layer_01.fill(pygame.Color(0,0,0,0));

createBoard = True;

while running:
    clock.tick(144);
    if pygame.event.get(pygame.QUIT):
        pygame.quit();
        running = False;
        exit();

    if pygame.event.get(pygame.MOUSEBUTTONDOWN):
        if pygame.mouse.get_pressed()[0] == True:
            mouse_pos = pygame.mouse.get_pos();
            mouse_pos_x = floor(mouse_pos[0]/scaled_cell_size);
            mouse_pos_y = floor(mouse_pos[1]/scaled_cell_size);
            layer_01.blit(red, (mouse_pos_x * scaled_cell_size, mouse_pos_y * scaled_cell_size));


    if createBoard == True:
        grid = create_board(screen_width_cells,screen_height_cells,cell_size)
        background.blit(grid, (0,0));
        background = pygame.transform.scale(background, (screen_width,screen_height));
        createBoard = False;


    layer_01 = pygame.transform.scale(layer_01, (screen_width, screen_height));

    screen.blit(background, (0,0));
    screen.blit(layer_01, (0,0));
    # screen.blit(hud_background,(0,scaled_cell_size*8));
    pygame.display.update();
