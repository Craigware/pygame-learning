import pygame
from math import floor

pygame.init();

screen_width = 1280; screen_height = 720;
screen = pygame.display.set_mode((screen_width,screen_height));
pygame.display.set_caption("Tile Choice");
screen.fill("gray27");

clock = pygame.time.Clock();

layer_01 = pygame.Surface((screen_width, screen_height)).convert_alpha();
layer_01.fill(pygame.Color(0,0,0,0));

cell_size = 32;
tileset = None;
current_row = 0; current_tile = 0;

running = True;

while running:
    if pygame.event.get(pygame.QUIT):
        pygame.quit();
        running = False;
        exit();

    for event in pygame.event.get():
        if event.type == pygame.MOUSEWHEEL and tileset != None:
            if event.y == 1:
                if current_tile == tileset_width_cells and current_row < tileset_height_cells:
                    current_row += 1;
                    current_tile = 0;
                elif current_tile != tileset_width_cells:
                    current_tile += 1;
            if event.y == -1:
                if current_tile == 0 and current_row > 0:
                    current_tile = tileset_width_cells;
                    current_row -= 1;
                elif current_tile != 0:
                    current_tile -= 1;
            print(f"current tile and row: {current_tile}, {current_row}");

        if event.type == pygame.DROPFILE:
            if event.file:
                tileset = pygame.image.load(event.file);
                tileset_height_cells = floor(tileset.get_height()/cell_size) - 1;
                tileset_width_cells = floor((tileset.get_width()/cell_size) - 1);

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                try:
                    print(f"tilemap height and width (0 indexed): {tileset_height_cells}, {tileset_width_cells}");
                except:
                    print("No tilemap has been selected");

    if tileset != None:
        tile = tileset.subsurface((cell_size*current_tile, cell_size*current_row, cell_size, cell_size));
        tile = pygame.transform.scale(tile, (cell_size*16, cell_size*16));
        layer_01.blit(tile, (0,0));
    screen.blit(layer_01,(0,0));
    pygame.display.update();
    clock.tick(144);
