import json, pygame

# def save_position(x,y):
#     data = (x,y);
#     with open("data/saved_position.json", "w", encoding='utf-8') as f:
#         json.dump(data, f, ensure_ascii=False, indent=4);
#     return "Saved the mouse position to data/saved_position.json"

# def load_position():
#     with open("data/saved_position.json") as f:
#         loaded_data = json.load(f)
#     return loaded_data;

def create_board(screen_width_cells, screen_height_cells, cell_size):
    grid = pygame.Surface((screen_width_cells*cell_size, screen_height_cells*cell_size));
    gray = pygame.Surface((cell_size, cell_size));
    white = pygame.Surface((cell_size, cell_size));
    white.fill("gray15");
    gray.fill("gray27")
    cell_y = 0;
    while cell_y < screen_height_cells:
        cell_x = 0;
        while cell_x < screen_width_cells:
            if cell_y % 2 == 0:
                if cell_x % 2 == 0:
                    grid.blit(gray, (cell_x*cell_size, cell_y*cell_size));
                else:
                    grid.blit(white, (cell_x*cell_size, cell_y*cell_size));
            else:
                if cell_x % 2 == 1:
                    grid.blit(gray, (cell_x*cell_size, cell_y*cell_size));
                else:
                    grid.blit(white, (cell_x*cell_size, cell_y*cell_size));
            cell_x += 1;
        cell_y += 1;
    return grid;
