import pygame

def move_target(key, target_position, amount):
    if key == pygame.K_d:
        target_position[0] += amount;
    if key == pygame.K_a:
        target_position[0] -= amount;
    if key == pygame.K_w:
        target_position[1] -= amount;
    if key == pygame.K_s:
        target_position[1] += amount;

def move_player(player_position, target_position, slide_speed):
    if player_position[0] > target_position[0]:
        player_position[0] -= slide_speed;
    elif player_position[0] < target_position[0]:
        player_position[0] += slide_speed;

    if player_position[1] > target_position[1]:
        player_position[1] -= slide_speed;
    elif player_position[1] < target_position[1]:
        player_position[1] += slide_speed;
