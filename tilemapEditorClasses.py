import pygame

class Vector2():
    def __init__(self, x, y):
        self.x = x;
        self.y = y;

class Tileset():
    def __init__(self, tilemap, cell_size):
        self.tilemap = tilemap;
        self.cell_size = cell_size;

    def cell_dimensions(self):
        x = round((self.tilemap.get_width()/self.cell_size) - 1);
        y = round((self.tilemap.get_height()/self.cell_size) - 1);
        return Vector2(x,y);
